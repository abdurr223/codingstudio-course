<?php
// tanpa encapsulation
// class Fruit{
//     public $name;
//     public $color;

//     function introduce(){
//         return $this->name . " - " . $this->color;
//     }
// }
// $apple = new Fruit(); 
// $apple->name ="Apel";
// $apple->color ="Merah";
// echo $apple->introduce();


// with encapsulation
class Fruit{
    private $name;
    private $color;

    function introduce(){
        return $this->name . " - " . $this->color;
    }
    function set_name($name){
        $this->name=$name;
    }

    function get_name(){
        return $this->name;
    }
     function set_color($color){
        $this->color=$color;
    }

    function get_color(){
        return $this->color;
    }
}

$apple = new Fruit(); 
// $apple->name ="Apel";
// $apple->color ="Merah";

echo $apple->introduce();
$apple->set_name("Apel");
$apple->set_color("Merah");

echo $apple->get_name();
echo " ";
echo $apple->get_color();

?>